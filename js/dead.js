




function dead(Joueur,mainGamePage,menu,infoPlayer,information,tabMonster,monstres,toursEnAttente,tours){
	var timer = setInterval(function(){

		if(Joueur.life<=0){
			

			$("body").append('<div class="reset"></div>');

			$(".mainGamePage .parcour").children().remove();
			$(".mainGamePage .toursEnJeu").children().remove();
			$(".mainGamePage .monstresEnJeu").children().remove();
			$(".informationPreGame").remove();

			$(".mainGamePage").unbind();
			$(".mainGamePage").remove();
			
			$("body").append('<div class="GameOver" style=""></div>');
			$(".GameOver").hide();

			var htmlOver = '<div class="pseudodie"><span>'+infoPlayer.name+'</span>you died</div>';
			htmlOver += '<div class="ptsdie">Your score is : <span>0</span>pts</div>';
			htmlOver += '<div class="textdie">Press "ENTER" to return to the menu</div>';

			$(".GameOver").append(htmlOver);
			$(".GameOver").fadeIn(500);

			tabMonster = [];
			monstres = [];
			toursEnAttente = [];
			tours = [];

			toMenuinDie(Joueur,mainGamePage,menu,infoPlayer,information)

			clearInterval(timer);

		}

	},1);
}

function toMenuinDie(Joueur,mainGamePage,menu,infoPlayer,information){
	var timer = 0;
	var pts = parseInt(Joueur.level*(1+infoPlayer.difficulty*5)+(Joueur.money/500))*10;

	var temppts = 0;
	var framedrop = 1;
	var frame = 0;
	var ok = 0;

	$('body').keyup(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13' && timer>10 && ok==0){   

	    	ok = 1; 	

	    	if(infoPlayer.bestscore<pts){
	    		infoPlayer.bestscore = pts;
	    	}
    		
    		$(".GameOver").remove();
        	inMenu(mainGamePage,menu,infoPlayer,information);        	
	    	
	    }
	});

	
	var set = setInterval(function(){
		timer++;
		frame--;
		if(frame<=0){
			if(temppts<pts){
			
			temppts++;
			$(".GameOver .ptsdie > span").text(temppts);

			}else{

				clearInterval(set);
			}
			frame=framedrop;
		}
		
		
			
		
		
	},1)
	
}
