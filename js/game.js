$(function() {
		
});

function startgame(infoPlayer,mainGamePage,menu,information){



	var TailleTourCss = 45; //taille des sprites dans le jeu 
	var compteChargement = {po : false};

	var	Joueur = {
			money      : 200, //(200 de base)
			life       : 20,
			difficulty : infoPlayer.difficulty, // 1 , 2 ou 3 (1 = facile , 3 = dur)
			level      : 0, //vague monstre
			time       : 20, //temps avant les vagues (20 de base)
			monsterswave: 5, //nombre de monstre pour la premiere vague (5 de base)
			wavetime : 30,
	}; 

	var	Parcours = {
			img  : "sprites/dalle.png",
			offset: [200,0], //Decallage du parcour par rapport au TOP et au LEFT de la page
			course: [['down' ,5],['right' ,7],['up',4],['right',4],['down',7],['left',10],['down',3]] // [direction, Nombre de carre]
	};
	
	var tabMonster = [];
	var tours = [];
	var toursEnAttente = [];
	var monstres = [];
	parcourPosition = traceRoute(TailleTourCss,Parcours);
	toursDansMenu(TailleTourCss,Joueur,parcourPosition,tours,compteChargement,toursEnAttente);
	initJoueur(Joueur);
	afficherRayonAction();
	supprimerTour(tours,Joueur,toursDisponibles());
	vagueMonster(Joueur,tabMonster,parcourPosition,TailleTourCss);

	verifMonsterTower(Joueur,tabMonster,toursDisponibles(),TailleTourCss);

	dead(Joueur,mainGamePage,menu,infoPlayer,information,tabMonster,monstres,toursEnAttente,tours);
}

function traceRoute(TailleTourCss,Parcours){

	var htmlDalles = "";	
	var thisPosition = [0+Parcours.offset[0],0+Parcours.offset[1]];
	var toutesPositionsDalles = [];
	var parcourPosition = [];
	//--------Savoir le nombre de dalles dans le parcour--------
	var nombreDalles = 0;
	for(var i = 0; i < Parcours.course.length;i++){
		nombreDalles+=Parcours.course[i][1];
	}
	//----------------------------------------------------------
	
	for(var i = 0; i < Parcours.course.length;i++){
		if(Parcours.course[i][0]=="up"){
			for(var ip = 0; ip < Parcours.course[i][1]-1;ip++){					
				htmlDalles+='<div class="dalleParcour" style="width:'+(TailleTourCss+TailleTourCss/3)+'px; height:'+(TailleTourCss+TailleTourCss/3)+'px; top:'+thisPosition[1]+'px; left:'+thisPosition[0]+'px;"><img src="'+Parcours.img+'"></div>';
				parcourPosition.push(thisPosition[0]+"-"+thisPosition[1]);
				thisPosition[0] = thisPosition[0];
				thisPosition[1] = thisPosition[1]-(TailleTourCss+TailleTourCss/3);
			}
		}

		if(Parcours.course[i][0]=="down"){
			for(var ip = 0; ip < Parcours.course[i][1]-1;ip++){					
				htmlDalles+='<div class="dalleParcour" style="width:'+(TailleTourCss+TailleTourCss/3)+'px; height:'+(TailleTourCss+TailleTourCss/3)+'px; top:'+thisPosition[1]+'px; left:'+thisPosition[0]+'px;"><img src="'+Parcours.img+'"></div>';
				parcourPosition.push(thisPosition[0]+"-"+thisPosition[1]);
				thisPosition[0] = thisPosition[0];
				thisPosition[1] = thisPosition[1]+(TailleTourCss+TailleTourCss/3);
			}
		}

		if(Parcours.course[i][0]=="left"){
			for(var ip = 0; ip < Parcours.course[i][1]-1;ip++){					
				htmlDalles+='<div class="dalleParcour" style="width:'+(TailleTourCss+TailleTourCss/3)+'px; height:'+(TailleTourCss+TailleTourCss/3)+'px; top:'+thisPosition[1]+'px; left:'+thisPosition[0]+'px;"><img src="'+Parcours.img+'"></div>';
				parcourPosition.push(thisPosition[0]+"-"+thisPosition[1]);
				thisPosition[0] = thisPosition[0]-(TailleTourCss+TailleTourCss/3);
				thisPosition[1] = thisPosition[1];
			}
		}

		if(Parcours.course[i][0]=="right"){
			for(var ip = 0; ip < Parcours.course[i][1]-1;ip++){					
				htmlDalles+='<div class="dalleParcour" style="width:'+(TailleTourCss+TailleTourCss/3)+'px; height:'+(TailleTourCss+TailleTourCss/3)+'px; top:'+thisPosition[1]+'px; left:'+thisPosition[0]+'px;"><img src="'+Parcours.img+'"></div>';
				parcourPosition.push(thisPosition[0]+"-"+thisPosition[1]);
				thisPosition[0] = thisPosition[0]+(TailleTourCss+TailleTourCss/3);
				thisPosition[1] = thisPosition[1];
			}
		}
		

	}

	$(".mainGamePage .parcour").html(htmlDalles);
	return(parcourPosition);

};

function toursDisponibles(){
	var toursDisponibles=[];

	var TowerN1 = {
		dist : 150, //rayon d'action
		type : 'Charon', //nom
		id   : 1, 
		img  : 'sprites/towerN1.png', 
		imgb : 'sprites/buildN1.png',
		time : 100, //temps de construction
		money: 70, //son prix
		sizeX: 60, 
		sizeY: 60,
		damage: 0.20, //attaque
	};

	var TowerN2 = {
		dist : 200, //rayon d'action
		type : 'Apollon', //nom
		id   : 2, 
		img  : 'sprites/towerN2.png', 
		imgb : 'sprites/buildN2.png',
		time : 300, //temps de construction
		money: 100, //son prix
		sizeX: 60, 
		sizeY: 60,
		damage: 0.25, //attaque
	};

	var TowerN3 = { 
		dist : 275, //rayon d'action
		type : 'Gelateur', //nom
		id   : 3, 
		img  : 'sprites/towerN3.png',
		imgb : 'sprites/buildN3.png', 
		time : 700, //temps de construction
		money: 550, //son prix
		sizeX: 60, 
		sizeY: 60,
		damage: 0, //attaque
	};

	var TowerN4 = {
		dist : 350, //rayon d'action
		type : 'Achlys', //nom
		id   : 4, 
		img  : 'sprites/towerN4.png', 
		imgb : 'sprites/buildN4.png',
		time : 1200, //temps de construction
		money: 1200, //son prix
		sizeX: 60, 
		sizeY: 60,
		damage: 1, //attaque
	};

	var TowerN5 = {
		dist : 400, //rayon d'action
		type : 'Déméter', //nom
		id   : 5, 
		img  : 'sprites/towerN5.png',
		imgb : 'sprites/buildN5.png', 
		time : 2000, //temps de construction
		money: 3000, //son prix
		sizeX: 60, 
		sizeY: 60,
		damage: 2, //attaque
	};

	toursDisponibles.push(TowerN1);
	toursDisponibles.push(TowerN2);	
	toursDisponibles.push(TowerN3); //Le numéro 3 TOUJOURS GLACE 
	toursDisponibles.push(TowerN4);
	toursDisponibles.push(TowerN5);

	return toursDisponibles;
}

function toursDansMenu(TailleTourCss,Joueur,parcourPosition,tours,compteChargement,toursEnAttente){
	var lesTours=toursDisponibles();

	$('.mainGamePage .menuTower').html('');

	var htmlTours="";
	for(var i=0;i<lesTours.length;i++){
		htmlTours+='<div class="Tower '+lesTours[i].id+'"><img src="'+lesTours[i].img+'"></div>'
	}

	$('.mainGamePage .menuTower').append(htmlTours);

	//--------Controle du CSS eb fonction de la taille des tours dans le menu--------
	$('.mainGamePage .infoTower').css("left",TailleTourCss+(TailleTourCss/3))
	$('.mainGamePage .menuTower').css("width",TailleTourCss+(TailleTourCss/3));  //Changer la largeur du menu de tours
	$('.mainGamePage .Tower').css("width",TailleTourCss).css("height",TailleTourCss).css("margin-top",TailleTourCss/4);
	//-------------------------------------------------------------------------------

	infosTours(lesTours,TailleTourCss);
	deplacementTour(lesTours,TailleTourCss,Joueur,parcourPosition,tours,compteChargement,toursEnAttente);
}

function infosTours(lesTours,TailleTourCss){
	$('.mainGamePage .Tower').on({
	    mouseenter: function () { //si la sourie passe au-dessus d'une tour
	    	var idTower = parseInt(($(this).attr("class")).replace("Tower ",""))-1;
	    
	    	$(".infoTower .Towername span").html(lesTours[idTower].type);    //Nom de la tour
	    	$(".infoTower .Towerprice span").html(lesTours[idTower].money);  //Prix de la tour
	    	$(".infoTower .Distanceshot span").html(lesTours[idTower].dist); //Distance de tirs
	    	$(".infoTower .Dammageshot span").html(lesTours[idTower].damage);//Dommage infligé
	    	$(".infoTower .Timetobuild span").html(lesTours[idTower].time);  //Temps de construction


	    	$(".infoTower").css("top",(((TailleTourCss/4)*(idTower+1))+TailleTourCss*idTower)+parseInt($("body").scrollTop())); //change la position de l'info de la tout en fonction de la position de la tour selectionne
	    	$(".infoTower").css("left",TailleTourCss+(TailleTourCss/3)+parseInt($("body").scrollLeft()));
	    	//-----------Affiche et cache les infos en fonction de la visibilité des tours dans le menu-----------
	    	/*var opacityTower = parseFloat($(this).css("opacity"));
	    	if(opacityTower < 1){

	    		$(".infoTower").css("visibility","hidden");
	    	}else{
	    		$(".infoTower").css("visibility","visible");
	    	}*/
	    	//----------------------------------------------------------------------------------------------------
	    	
	    	$(".infoTower").fadeIn(10); //affiche les infos de la tour
	    },

	    mouseleave: function () { //si la sourie n'est pas au-dessus d'une tour
	       $(".infoTower").fadeOut(10); //chache les infos de la tour	      
	       
	    }
	});
	
	
}

function deplacementTour(lesTours,TailleTourCss,Joueur,parcourPosition,tours,compteChargement,toursEnAttente){
	

	var possibiliteCreer = false; //savoir si on peux ajouter une tour
	var tourelectionnee = 0; //recupère l'id de la tour selectionnée
	var testmouvement = 0;
	
	var timerrr = setInterval(function() {

		if($(".reset").length !=0){
				clearInterval(timerrr);
				console.log("ok");
			}

		for(var i = 0; i<lesTours.length;i++){
			if(Joueur.money < lesTours[i].money){
				var cetteTourHtml = ".mainGamePage .menuTower ."+(i+1);			
				$(cetteTourHtml).css("opacity","0.5");
			}
			else{
				var cetteTourHtml = ".mainGamePage .menuTower ."+(i+1);			
				$(cetteTourHtml).css("opacity","1");
			}
		};
	},10)

	$(document).keyup(function(e) { //si l'utilisateur appui sur 'ECHAP'
		if (e.keyCode === 27) {
			possibiliteCreer  = false; //ne peut plus poser de tour
			$('.mainGamePage .tourSelect').css('display', 'none'); //le selecteur de tour se chache
			$('.mainGamePage .tourSelect').css('top',-100); //le selecteur se cache en dehors de la page
			$('.mainGamePage .menuTower .Tower').css('opacity',1); //toute les tours redeviennent visible
			$(".mainGamePage .rayon").css('display', 'none');	
			testmouvement = 0;	
		}
	});


	$('.mainGamePage').on('click', '.Tower',function() { //si l'utilisateur clique sur une tour
		var idTowerSelect =  parseInt(($(this).attr("class")).replace("Tower ",""))-1; //récupère l'id de la tour selectionnee

		if(possibiliteCreer==false){ 
			if(Joueur.money>=lesTours[idTowerSelect].money){ // si l'utilisateur à suffisament d'argent pour la tour
				possibiliteCreer = true;
				tourelectionnee = idTowerSelect;

				for(var i = 0; i<lesTours.length;i++){
					
					//-------------Cache toutes les autres tours-------------
					if(idTowerSelect != lesTours[i].id-1){		
					
						var cetteTourHtml = ".mainGamePage .menuTower ."+(i+1);			
						$(cetteTourHtml).css("opacity","0.5");
						$(".mainGamePage .infoTower").css("display","none");
						$(".mainGamePage .rayon").fadeIn(0);
						$(".mainGamePage .rayon").css("width",(lesTours[tourelectionnee].dist*TailleTourCss)/58).css("height",(lesTours[tourelectionnee].dist*TailleTourCss)/58);
						$(".mainGamePage .tourSelect").fadeIn(0);
						$(".mainGamePage .tourSelect img").attr("src",lesTours[tourelectionnee].img);
						$(".mainGamePage .tourSelect span").text(lesTours[tourelectionnee].type)
						$(".mainGamePage .tourSelect").css("width",TailleTourCss+(TailleTourCss/3)).css("height",TailleTourCss+(TailleTourCss/3));

					}
					//-------------------------------------------------------
				}
				
			}
			else{
				possibiliteCreer = false;
				tourelectionnee = 0;
				testmouvement = 0;
			}
			//console.log(possibiliteCreer,tourelectionnee);
		}


	});

	$('.mainGamePage').click(function(e){	

			if(possibiliteCreer==true && testmouvement == 1 && (e.pageX)>TailleTourCss*2 && verifPosSurDalle(e.pageX,e.pageY,TailleTourCss,parcourPosition)==true){
				
				var htmlAjoutTour = "";

				htmlAjoutTour += '<div class="tourJeu id'+tours.length+' type'+lesTours[tourelectionnee].id+'">';
                htmlAjoutTour +=    '<div class="flexRayonJeu">';
                htmlAjoutTour +=        '<div class="rayonJeu"></div>';
                htmlAjoutTour +=        '<img src="'+lesTours[tourelectionnee].imgb+'">';
                htmlAjoutTour +=    '</div>';
                htmlAjoutTour +=    '<span></span>';
                htmlAjoutTour +=    '<div class="chargement"></div>';
                htmlAjoutTour += '</div>';		
                
                

				$('.mainGamePage .toursEnJeu').append(htmlAjoutTour);

				$('.mainGamePage .toursEnJeu .id'+tours.length).css("width",(TailleTourCss+TailleTourCss/3)).css("height",(TailleTourCss+TailleTourCss/3));
				$('.mainGamePage .toursEnJeu .id'+tours.length).css("top",(e.pageY)-TailleTourCss/2).css("left",(e.pageX)-TailleTourCss/2);
				$('.mainGamePage .toursEnJeu .id'+tours.length+' .rayonJeu').css("width",(lesTours[tourelectionnee].dist*TailleTourCss)/58).css("height",(lesTours[tourelectionnee].dist*TailleTourCss)/58);

				Joueur.money -= lesTours[tourelectionnee].money;
				$(".mainGamePage .infoPlayer .money span").html(Joueur.money);



				

				possibiliteCreer = false;
				$('.mainGamePage .tourSelect').css('display', 'none'); //le selecteur de tour se chache
				$('.mainGamePage .tourSelect').css('top',-100); //le selecteur se cache en dehors de la page
				$('.mainGamePage .menuTower .Tower').css('opacity',1); //toute les tours redeviennent visible
				$(".mainGamePage .rayon").css('display', 'none');	
				testmouvement = 0;

				tours.push(tourelectionnee);	
				
				toursEnAttente.push([tourelectionnee,tours.length]);
				chargementTour(lesTours,tours,toursEnAttente,compteChargement);
			}

	});

	$(".mainGamePage").mousemove(function(e){
		if(possibiliteCreer==true){
			var tailleRayon = parseInt(($(".mainGamePage .rayon").css("width")).replace("px",""));
			$(".mainGamePage .tourSelect").css("top",(e.pageY)-TailleTourCss/2).css("left",(e.pageX)-TailleTourCss/2);

			if(verifPosSurDalle(e.pageX,e.pageY,TailleTourCss,parcourPosition)==false){
				$(".mainGamePage .tourSelect").css("opacity","0.5");
				$(".mainGamePage .tourSelect").css('cursor', 'not-allowed');
			}
			else{
				$(".mainGamePage .tourSelect").css("opacity","1");
				$(".mainGamePage .tourSelect").css('cursor', 'none');
			}

			testmouvement = 1;
		}
	});

	


}
 

function chargementTour(lesTours,tours,toursEnAttente,compteChargement){
	
	
	
	var timer = setInterval(function(){
		if($(".reset").length !=0){
				clearInterval(timer);
				console.log("ok");
			}
		if(compteChargement.po == false && toursEnAttente.length>0){
			compteChargement.po = true;
			
			compte = 0;

			var tempsDeConstruction = lesTours[toursEnAttente[0][0]].time;
			
			var timerTour = setInterval(function(){
				if($(".reset").length !=0){
					clearInterval(timerTour);
					console.log("ok");
				}

				idTourEnAttente = toursEnAttente[0][1]-1;
				nTourEnAttente = toursEnAttente[0][0];
				
				if (compte <= tempsDeConstruction) {
					
					
					$('.ToursEnJeu .id'+(idTourEnAttente)+' .chargement').css("width",((100*compte)/tempsDeConstruction)+"%");
					compte++;				
				
				}
				else {	
					$('.ToursEnJeu .id'+(idTourEnAttente)+' .chargement').fadeOut(0);
					$('.ToursEnJeu .id'+(idTourEnAttente)+' span').html(lesTours[nTourEnAttente].type);
					$('.ToursEnJeu .id'+(idTourEnAttente)+' img').attr("src",lesTours[nTourEnAttente].img);

					toursEnAttente.splice(0,1);

					compteChargement.po = false;	
					clearInterval(timerTour);	
					
				}

			},1);

		}
		else{
			clearInterval(timerTour);
		}
		
		},1);
	
}

function verifPosSurDalle(x,y,TailleTourCss,parcourPosition){ 
	var inParcPos = true;

	for(var i = 0; i < parcourPosition.length; i++){ //verifie si la tour n'est pas sur une dalle
		xParcPos = parseInt((parcourPosition[i].split("-"))[0]);
		yParcPos = parseInt((parcourPosition[i].split("-"))[1]);
		if(x>xParcPos-(TailleTourCss+TailleTourCss/3)/2 && x<xParcPos+(TailleTourCss+TailleTourCss/3)+15 && y>yParcPos-(TailleTourCss+TailleTourCss/3)/2 && y<yParcPos+(TailleTourCss+TailleTourCss/3)+15){
			inParcPos = false;
		}
	}

	if(($(".toursEnJeu").children()).length>0){ //verifie si la tour n'est pas sur une tour
		for(var i = 0; i <$(".toursEnJeu").children().length;i++){			
			temptour = $(".toursEnJeu").children("div")[i]
			temptourPos = $(temptour).position();
			xTourPos = temptourPos.left;
			yTourPos = temptourPos.top;

			if(x>xTourPos-(TailleTourCss+TailleTourCss/3)/2 && x<xTourPos+(TailleTourCss+TailleTourCss/3)+15 && y>yTourPos-(TailleTourCss+TailleTourCss/3)/2 && y<yTourPos+(TailleTourCss+TailleTourCss/3)+15){
				inParcPos = false;
			}
		}		
	}

	return(inParcPos);
}


function afficherRayonAction(){ //Apparaitre - disparaitre le rayon de la tour
	$(".toursEnJeu").on('click', '.tourJeu', function(event){ //quand le joueur clique sur une tour
	    temptour = $(this).attr("class"); //recupere la tour selectionnée
	    temptour = temptour.split(" "); //recuperer la valeur de la class en string
	    recherche= ".toursEnJeu ."+temptour[1]+ " .rayonJeu" //variable qui prend la valeur de du 2eme nom de la class
	   
	    if($(recherche).css("opacity")>0.1){ 
	    	$(recherche + ".rayonJeu").css("opacity",0);
	    	$(recherche + ".rayonJeu").css("visibility","hidden")
	    }
	    else{
	    	$(recherche + ".rayonJeu").css("opacity",0.4);
	    	$(recherche + ".rayonJeu").css("visibility","visible")
	    }
	});
}


function supprimerTour(tours,Joueur,toursDisponibles){

	$(document).on("mousedown", ".tourJeu img", function(e) {

    	if( e.which == 2 ) { //detecte le clique du milieu
    		
    		temptour = $($(this).parent().parent()).attr("class"); //recupere la tour selectionnée
	    	temptour = temptour.split(" "); //recuperer la valeur de la class en string
	    	recherche= ".toursEnJeu ."+temptour[1]; //variable qui prend la valeur de du 2eme nom de la class
	    	type = temptour[2]; //recupere le type de la tour selectionée
	    	type = parseInt(type.replace("type",""))-1; //transforme son type en ID pour le trouver dans l'objet Tours
	    	console.log(tours)

	    	if($(recherche + " .chargement").css('display')=="none"){ //Verifie que la tour à fini d'être construite

	    		
	    		 //retire la tour du tableau de tour disponible
				$(recherche).remove(); //supprime la tour du HTML
	    		Joueur.money += toursDisponibles[type].money/2; //Joueur récupere la moitier de la valeur de la tour
				$(".mainGamePage .infoPlayer .money span").html(Joueur.money);

	    	}
	    	
   		}

   });
}


function initJoueur(Joueur){


	//--------Initialisation des infos du joueur--------
	$(".mainGamePage .infoPlayer .life span").html(Joueur.life); //init la vie
	$(".mainGamePage .infoPlayer .money span").html(Joueur.money); //init l'argent
	$(".mainGamePage .infoPlayer .wave span").html(Joueur.level); //init la vague
	$(".mainGamePage .infoPlayer .time span").html(Joueur.time); //init le temps
	//--------------------------------------------------	
}

