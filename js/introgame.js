$(function() {

	var infoPlayer = {
		name:"",
		difficulty:1,
		bestscore:0,
	};

	var mainGamePage = $(".mainGamePage").children();
	$(".mainGamePage").remove(); //supprimer le div enfant de la pge du jeu
	//$("body").append(' <div class="mainGamePage"></div>')
	//$(".mainGamePage").html(mainGamePage); //creer la page du jeu
	//startgame(); //lancement du jeu
	//$(".intro").remove();

	var information=$(".informationPreGame").children();
	$(".informationPreGame").remove();

	var menu= $(".menu").children();
	$(".menu").remove();

	var intro = {i:0,};

	animationIntro(intro,mainGamePage,menu,infoPlayer,information);

	//inMenu(mainGamePage,menu,infoPlayer);
	
});

function animationIntro(intro,mainGamePage,menu,infoPlayer,information){

	$(".blocblack").remove();

	var anim = 0;

	var topTitle = ($(".intro .iname").css("height")).replace("px","");
	var leftTitle = ($(".intro .iname").css("width")).replace("px","");
	var widthLogo = ($(".intro .logo").css("width")).replace("px","");
	var paddingTitle = parseInt(topTitle)+20;
	var opacLogo = 0;
	var textInformation = "by Elora CHEVALLIER, Sami BELABACI, Zina MPEMBA, Liantsoa TSIKY, Sylvain DENDELE";
	var textInformationEmpty =""
	var cara = 0

	var framejump = 15;
	var frame = 0;

	var wait = 750;
	var waitt = 10;
	var etatPress = 0;

	var finalwait = 0;

	var timer = setInterval(function(){
		

		var docX = $( window ).width();
		var docY = $( window ).height();

		var posTitX = (docX/2)-(leftTitle/2)
		var posTitY = (docY/2)-(topTitle/2);

		var posLogY = (posTitY+topTitle/2+widthLogo/2)+40

		$(".intro .iname").css("top",posTitY);
		$(".intro .iname").css("left",posTitX);

		$(".intro .logo").css("top",posLogY);
		$(".intro .logo").css("left",(docX/2)-widthLogo/2);
		$(".intro .logo").css("opacity",opacLogo);

		$(".intro .information").css("top",posLogY+widthLogo/2+40);

		$(".intro .iname span").css("padding-top",paddingTitle+"px");

		

		if(paddingTitle>0 && anim == 0){
			paddingTitle-=0.5;
		}
		if(paddingTitle<=0 && anim == 0){
			paddingTitle=0;
			anim = 1;
		}



		if(opacLogo<1 && anim == 1){
			opacLogo+=0.005;
		}
		if(opacLogo>=1 && anim == 1){
			opacLogo = 1;
			anim = 2;
		}

		if(textInformationEmpty.length<textInformation.length && anim == 2){			
			if(frame>=framejump){
				if(textInformationEmpty.length<textInformation.length){					
					textInformationEmpty+=textInformation[cara];	
					$(".intro .information span").text(textInformationEmpty);				
					cara++;	
				}
				frame=0;
			}	
			frame++;
		}

		if(textInformationEmpty.length==textInformation.length && anim == 2){
			if(wait>0){
				wait--;
			}
			else{
				anim=3;
			}
		}

		if(anim == 3){
			$(".intro .information ").fadeOut(500);
			anim=4;
			wait=500;
		}

		if(anim==4){
			if(wait>0){
				wait--;
			}
			else{
				anim=5;
				$(".intro .information span").text('Press "ENTER"');				
			}
		}

		if(anim==5){
			if(intro.i==0){
				intro.i=1;
				toMenu(intro,mainGamePage,menu,infoPlayer,information);
				
			}			
			
			if(etatPress==0){
				if(wait>0){
					wait--;
				}
				else{
					etatPress=1;
					$(".intro .information ").fadeIn(200);	
					wait=201;		
				}
			}
			else{
				if(wait>0){
					wait--;
				}
				else{
					etatPress=0;
					$(".intro .information ").fadeOut(200);	
					wait=201;					
				}
			}
		}

		if(intro.i==2){

			if(finalwait == 0){
				waitt = 500;
				$(".intro").fadeOut(500);
				finalwait=1;
			}

			if(waitt>0 && finalwait==1){
				waitt--;
			}else{
				clearInterval(timer);
				$(".intro").remove();
				inMenu(mainGamePage,menu,infoPlayer,information);
			}
			
			
	    	
			//;
		}


	},1);
}

function toMenu(intro,mainGamePage,menu,information){
	
	$('body').keyup(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13' && intro.i==1){    	
    		
        	intro.i=2;  

        	
	    	
	    }
	});
	
	
}