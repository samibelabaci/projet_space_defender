


function inMenu(mainGamePage,menu,infoPlayer,information){
	$(".reset").remove();
	$(".intro").remove();

	$("body").append(' <div class="menu"></div>')
	$(".menu").html(menu); //creer la page du jeu

	$(".menu .difficulty .slider").text(infoPlayer.difficulty*2+"/6");
	$(".menu .ready .bestscore>span").text(infoPlayer.bestscore);
	$(".menu").fadeIn(500);
	
	if(infoPlayer.bestscore!=0){
		$(".menu .difficulty>span").text("Difficulty :");
		$(".menu .pseudo input").val(infoPlayer.name);
	}

	chooseDifficulty(infoPlayer);
	toPlay(infoPlayer,mainGamePage,information,menu);
}

function chooseDifficulty(infoPlayer){

	var couleur1 = [1,255,1];
	var couleur2 = [255,1,1];
	var diffcouleur = [couleur1[0]-couleur2[0],couleur1[1]-couleur2[1],couleur1[2]-couleur2[2]];
	var tempdiff = (infoPlayer.difficulty*2)
	var tempcouleur = [couleur1[0]-(diffcouleur[0]*(tempdiff/6)),couleur1[1]-(diffcouleur[1]*(tempdiff/6)),couleur1[2]-(diffcouleur[2]*(tempdiff/6))]
	$(".menu .difficulty .slider").css("background-color","rgb("+tempcouleur[0]+","+tempcouleur[1]+","+tempcouleur[2]+")");
	

	$(document).on("mousedown", ".slider", function(e) {
		$(".menu .difficulty>span").text("Difficulty :");
	});

	$(document).on("mousemove", ".slider", function(e) {

		

		if( e.which == 1 ) {

			var docX = $( window ).width();			
			var docY = $( window ).height();

			
			var pos = $(this).position();
			var posX = pos.left;
			var posY = pos.top;
			var sizeX = $(this).width();
			var mouseX = e.pageX;
			var mouseY = e.pageY;

		

			var diffi = ((sizeX+posX)-mouseX)*5/sizeX;
			diffi = 5-diffi+1;

			var couleur3 = [couleur1[0]-(diffcouleur[0]*(diffi/6)),couleur1[1]-(diffcouleur[1]*(diffi/6)),couleur1[2]-(diffcouleur[2]*(diffi/6))]
		

			$(".menu .difficulty .slider").text((Number((diffi).toFixed(2)))+"/6");
			$(".menu .difficulty .slider").css("background-color","rgb("+couleur3[0]+","+couleur3[1]+","+couleur3[2]+")")

			infoPlayer.difficulty = diffi/2;
		}

	});
};

function toPlay(infoPlayer,mainGamePage,information,menu){
	$( ".menu .ready .btn-play" ).click(function() {
  		if($(".menu .pseudo input").val()==""){
  			alert("Enter your pseudo");
  		}
  		else{
  			if(infoPlayer.bestscore!=0){
  				infoPlayer.name = $(".menu .pseudo input").val();
	  			$(".menu").remove();
	  			
	  			$("body").append(' <div class="mainGamePage"></div>')
				$(".mainGamePage").html(mainGamePage); //creer la page du jeu
				startgame(infoPlayer,mainGamePage,menu,information);
  			}
  			else{
  				informationBeforePlay(infoPlayer,mainGamePage,information,menu)
  			}
		}
	});
}

function informationBeforePlay(infoPlayer,mainGamePage,information,menu){

	infoPlayer.name = $(".menu .pseudo input").val();
	$(".menu").fadeOut(500);
	var wait=0;
	var anim=0;
	var timer = setInterval(function(){

		if($(".reset").length !=0){
				clearInterval(timer);
				console.log("ok");
			}

		if(wait>500&&anim==0){
			$(".menu").remove();
			wait=0;
			anim=1;
			$("body").append(' <div class="informationPreGame"></div>');
			$(".informationPreGame").fadeOut(1);
			$(".informationPreGame").html(information);
			$(".informationPreGame").fadeOut(1);
			$(".informationPreGame").fadeIn(500);
		}
		if(wait>750&&anim==1){
			wait=0;
			anim=2;
			$(".informationPreGame").fadeOut(500);
		}
		if(wait>500&&anim==2){

			wait=0;
			anim=3;

			$("body").append(' <div class="mainGamePage"></div>')
			$(".mainGamePage").html(mainGamePage); //creer la page du jeu
			startgame(infoPlayer,mainGamePage,menu,information);
			clearInterval(timer);
		}

		wait++;
	},1);

	
	

	//$("body").append(' <div class="mainGamePage"></div>')
	//$(".mainGamePage").html(mainGamePage); //creer la page du jeu
	//startgame(infoPlayer);
}