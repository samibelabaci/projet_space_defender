$(function(){

})


function vagueMonster(Joueur,tabMonster,parcourPosition,TailleTourCss){
	backgroundImage();
	Joueur.time = Joueur.time/(Joueur.difficulty);

	var timer = (Joueur.time+1)*250;
	var pause = true;
	var nbmonster = Joueur.monsterswave;
	var idmonster = 0;
	var nbTotalmonster = 0;
	var timeperwave = 0;
	var gnenerateMonster = true;
	
	var tempMonster;
	var xFirstDalle = parseInt(parcourPosition[0].split("-")[0]);
	var yFirstDalle = parseInt(parcourPosition[0].split("-")[1]);
	var hpMonster = 100;
	var moneyMonster = parseInt(hpMonster/5);

	$(".mainGamePage .infoPlayer .time span").css("color","green");



	var timermm = setInterval(function() { 
		
		if($(".reset").length !=0){
				clearInterval(timermm);
				console.log("ok");
			}

		if(pause==true){ // en pause
			
			if(timer>0){
			}
			else{ // fin de la pause, nouvelle vague
				
				Joueur.level+=1;
				pause = false;				
				gnenerateMonster = true;
				timer = (Joueur.wavetime+1)*250;
				timeperwave = timer;
				$(".mainGamePage .infoPlayer .time span").css("color","red");
				$(".mainGamePage .infoPlayer .wave span").html(Joueur.level);
			}
		}
		else{ //nouvelle vague
			if(timer>0){
				var timepermonster = parseInt(timeperwave/nbmonster);

				for(var i=nbmonster; i>0 ; i--){					
					if(timer == parseInt(timepermonster*i)-1 && gnenerateMonster == true){
						idmonster = i;
						nbTotalmonster++;

						//--------------------CREATION DU MONSTRE--------------------			
						
						var randomskin = [["alien","sprites/monsterN1"],["alien2","sprites/monsterN2"],["alien3","sprites/monsterN3"]]; //sprites differents pour les monstres
						var randomalien = Math.floor(Math.random() * randomskin.length);  
						var randomName = randomskin[randomalien][0];
						var randomImg  = randomskin[randomalien][1];

						var placementDiff = -(TailleTourCss+(TailleTourCss/3))+(parseInt((nbmonster-i+1)*-((TailleTourCss+(TailleTourCss/3))*0.8))/13)*Joueur.level;
						
						tempMonster = new Monster((placementDiff),xFirstDalle,randomName,randomImg+".png",randomImg+"glace.png",hpMonster,moneyMonster,nbTotalmonster,TailleTourCss); 
						//console.log(tempMonster.cStep) //etape sur la dalle
						tabMonster.push(tempMonster);
						
						//-----------------------------------------------------------
						var ajout = 1-Joueur.difficulty/2.75
						if(ajout<0){
							ajout=0;
						}
						hpMonster+=15*(ajout+Joueur.difficulty/2.75)+(nbTotalmonster*0.20);
						moneyMonster = parseInt((hpMonster/13)+(hpMonster*Joueur.difficulty/100));

						if(i==1){
							nbmonster+=2;
							gnenerateMonster = false;							
						}

						
					}
				}
				
			}
			else{ // fin de la nouvelle vague, retour en pause
				pause = true;
				timer = (Joueur.time+1)*250;
				Joueur.time+=2/Joueur.difficulty;
				$(".mainGamePage .infoPlayer .time span").css("color","green");
			}
		}




		$(".mainGamePage .infoPlayer .time span").html(parseInt(timer/250));

		timer--;
	},0);


	//---------------DEPLACEMENT MONSTRES---------------
	
	var tempsdecreation = setInterval(function(){

		if($(".reset").length !=0){
				clearInterval(tempsdecreation);
				console.log("ok");
			}

		

		for(var a=0, c=tabMonster.length;a<c;a++){

			

			if(tabMonster[a].cStep<parcourPosition.length){ //Le monstre bouge

				

				var xDalle = parseInt(parcourPosition[tabMonster[a].cStep].split("-")[0]);
				var yDalle = parseInt(parcourPosition[tabMonster[a].cStep].split("-")[1]);
				var xDiff = tabMonster[a].left-xDalle;
				var yDiff =	tabMonster[a].top-yDalle;
				
				//tabMonster[i].inglace=true
				//.framedrop
				if(yDiff>=0.5){
					if(tabMonster[a].inglace==true){
						if(tabMonster[a].framedrop > 0){
							tabMonster[a].top--;
							tabMonster[a].framedrop=0;
						}else{
							tabMonster[a].framedrop++;
						}
						
					}
					else{
						tabMonster[a].top--;
						tabMonster[a].framedrop = 0;
					}
					
					
				}
				if(yDiff<=-0.5){
					if(tabMonster[a].inglace==true){
						
						if(tabMonster[a].framedrop > 0){
							tabMonster[a].top++;
							tabMonster[a].framedrop=0;
						}else{
							tabMonster[a].framedrop++;
						}
					}
					else{
						tabMonster[a].top++;
						tabMonster[a].framedrop = 0;
					}
					
				}


				if(xDiff>=0.5){
					if(tabMonster[a].inglace==true){
						if(tabMonster[a].framedrop > 0){
							tabMonster[a].left--;
							tabMonster[a].framedrop=0;
						}else{
							tabMonster[a].framedrop++;
						}
					}
					else{
						tabMonster[a].left--;
						tabMonster[a].framedrop = 0;
					}
					
				}
				if(xDiff<=-0.5){
					if(tabMonster[a].inglace==true){
						if(tabMonster[a].framedrop > 0){
							tabMonster[a].left++;
							tabMonster[a].framedrop=0;
						}else{
							tabMonster[a].framedrop++;
						}
					}
					else{
						tabMonster[a].left++;
						tabMonster[a].framedrop = 0;
					}
					
				}


				if(Math.abs(xDiff)<0.5 && Math.abs(yDiff)<0.5){
					tabMonster[a].cStep++;
				}

				//if(a==0){console.log(tabMonster[a].cStep,xDiff,yDiff);}

				
				$(tabMonster[a].selecteurHTML).css('top',(tabMonster[a].top+tabMonster[a].sizediff)+'px');
				$(tabMonster[a].selecteurHTML).css('left',(tabMonster[a].left+tabMonster[a].sizediff)+'px');

			}


			if(tabMonster[a].cStep>=parcourPosition.length){//le monstre se supprime et le joueur perd de la vie				
				$(tabMonster[a].selecteurHTML).fadeOut(500);
				tabMonster.splice(a,1);
				Joueur.life--;
				$(".mainGamePage .infoPlayer .life span").html(Joueur.life);				
				break;
			}

		}




	},(20*(1/(((TailleTourCss*10)/58)/10)))/Joueur.difficulty);
	/* 20*(1/(((TailleTourCss*10)/58)/10)))/Joueur.difficulty <<<< vitesse du monstre qui varie en fonction de la taille 
	   des tours et de la difficulter */
} 

function Monster(top,left,name,img,imgG,hp,money,nbTotalmonster,TailleTourCss){
	this.top     = top;
	this.left    = left;
	this.leftTemp= 0;
	this.topTemp = top;
	this.name    = name;
	this.img     = img;
	this.imgG    = imgG; //glace
	this.hp      = hp;
	this.hpmax   = hp;
	this.cStep   = 0; //etape sur les dalles
	this.money   = money;
	this.id      = nbTotalmonster;
	this.size    = (TailleTourCss+(TailleTourCss/3))*0.8
	this.sizediff= Math.abs(this.size - (TailleTourCss+(TailleTourCss/3)))/2;
	this.inglace = false;
	this.oldglace= false;
	this.idtourglace=999;
	this.framedrop= 0;
	this.createMonster = function(){

		var html = $('<div style="top:'+(this.top+this.sizediff)+';left:'+(this.left+this.sizediff)+';width:'+this.size+';height:'+this.size+'" class="monstre idm'+this.id+'"><img src="'+this.img+'"><div><span class="lifeMonster">'+parseInt(this.hp)+' HP</span></div></div>');		

		this.selecteurHTML = html;

		$('.monstresEnJeu').append(html);

	}

	this.createMonster();
}


function verifMonsterTower(Joueur,tabMonster,toursDisponibles,TailleTourCss){

	var timervv = setInterval(function(){

		if($(".reset").length !=0){
				clearInterval(timervv);
				console.log("ok");
			}

		if(tabMonster.length>0 && $(".toursEnJeu").children().length>0){

			for(var i = 0; i<tabMonster.length;i++){				
				for(var t = 0; t<$(".toursEnJeu").children().length;t++){		

					tempTour = ($(".toursEnJeu").children()[t]);
					typeTour = parseInt($(tempTour).attr("class").split(" ")[2].replace("type",""));
					rechercheTour = $(tempTour).attr("class").split(" ")[1];					
					xpTour = parseInt($(tempTour).css("left").replace("px",""))+((TailleTourCss+TailleTourCss/3)/2);
					ypTour = parseInt($(tempTour).css("top").replace("px",""))+((TailleTourCss+TailleTourCss/3)/2);
					rayTour= (toursDisponibles[typeTour-1].dist*TailleTourCss)/58;
					damTour= toursDisponibles[typeTour-1].damage;

					xMoins = xpTour-(rayTour/2)
					xPlus  = xpTour+(rayTour/2)
					yMoins = ypTour-(rayTour/2)
					yPlus  = ypTour+(rayTour/2)

					xpMonster = tabMonster[i].left+(tabMonster[i].size/2);
					ypMonster = tabMonster[i].top+(tabMonster[i].size/2);
					
					marge = (tabMonster[i].size/2)*0.95; //on rajoute une marge

					if($(".toursEnJeu ." +rechercheTour+" .chargement").css('display')=="none"){ // si la tour à fini de charger
						if(typeTour!=3){
							if(xpMonster>=xMoins-marge && xpMonster<=xPlus+marge && ypMonster>=yMoins-marge && ypMonster<=yPlus+marge){ // si le monstre est dans le rayon de la tour
								if(tabMonster[i].hp>0){
									tabMonster[i].hp-=damTour/2;
									$(tabMonster[i].selecteurHTML).find("span").text(parseInt(tabMonster[i].hp)+" HP");
								}
								else{
									$(tabMonster[i].selecteurHTML).find("span").text("0 HP");
									$(tabMonster[i].selecteurHTML).fadeOut(500);
									Joueur.money+=tabMonster[i].money;
									$(".mainGamePage .infoPlayer .money span").html(Joueur.money);
									tabMonster.splice(i,1);
									break;
								}
							}
						}
						else{

							if(xpMonster>=xMoins-marge && xpMonster<=xPlus+marge && ypMonster>=yMoins-marge && ypMonster<=yPlus+marge){
								tabMonster[i].inglace=true; //en glace
								tabMonster[i].idtourglace = parseInt(rechercheTour.replace("id",""));
								if(tabMonster[i].inglace != tabMonster[i].oldlace){
									tabMonster[i].oldlace = tabMonster[i].inglace;
									$(tabMonster[i].selecteurHTML).find('img').attr("src",tabMonster[i].imgG);
								}

								
							}
							else{
								if(tabMonster[i].idtourglace == parseInt(rechercheTour.replace("id",""))){
									tabMonster[i].inglace=false;

									if(tabMonster[i].inglace != tabMonster[i].oldlace){
										tabMonster[i].oldlace = tabMonster[i].inglace;
										$(tabMonster[i].selecteurHTML).find('img').attr("src",tabMonster[i].img);
									}

								}
								
							}
						}
					

					}					

					else{
						continue;
					}
					
				}
			}

		}
		//console.log($(".toursEnJeu").children().length)


	},5)
}





